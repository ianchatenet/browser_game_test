const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
  {
    devtool: 'source-map',
    name: 'local',
    entry: './src/scripts/main.ts',
    mode: "development",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader',
          ],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    plugins: [new HtmlWebpackPlugin({ title: 'Browser game test' })],
    devServer: {
      host: '0.0.0.0',
      watchFiles: ['dist/**'],
      compress: true,
      port: 8000,
      allowedHosts: 'auto'
    },
    watchOptions: {
      poll: true // Or you can set a value in milliseconds.
    },
    stats: {
      assets: false,
      modules: false
    },
  },
  {
    name: 'ci',
    entry: './src/scripts/main.ts',
    mode: "production",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/,
        },
        {
          test: /\.css$/,
          use: [
            'style-loader',
            'css-loader',
          ],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
        },
      ],
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'public'),
    },
    plugins: [new HtmlWebpackPlugin({ title: 'Browser game test' })],
  }
];