/**
 * @jest-environment jsdom
 */

import { Sprite } from './sprite';
import { SpriteI } from './sprite.interface';
import { SpriteDrawDto } from './dtos/sprite.draw.dto';


describe('Sprite', () => {
  const data = {
    sourceImg: '',
    col: 0,
    row: 0,
    width: 16,
    height: 16,
    separator: 1,
    x: 0,
    y: 0,
  };

  const sprite: SpriteI = new Sprite({ sourceImg: data.sourceImg, col: data.col, row: data.row, width: data.width, height: data.height, separator: data.separator });
  const canvas: HTMLCanvasElement = global.document.createElement('canvas');
  /* eslint-disable @typescript-eslint/no-non-null-assertion */
  const ctx = canvas.getContext('2d')!;

  const spyDrawImage = jest.spyOn(ctx, 'drawImage');

  describe('#draw', () => {
    it('Should call draw', () => {
      // arrange
      const dto: SpriteDrawDto = { context: ctx, x: data.x, y: data.y, spriteWidth: data.width, spriteHeight: data.height };
      const img: HTMLImageElement = new Image();
      img.src = '';
      spyDrawImage.mockReturnValueOnce();
      // act
      sprite.draw(dto);
      // assert
      expect(spyDrawImage).toHaveBeenCalledTimes(1);
      expect(spyDrawImage).toHaveBeenCalledWith(img, data.col * data.width, data.row * data.height, data.width, data.height, dto.x, dto.y, dto.spriteWidth, dto.spriteHeight);
    });
  });
});