export type SpriteDto = {
  sourceImg: string;
  col: number;
  row: number;
  width: number;
  height: number;
  separator: number
}