export type SpriteDrawDto = {
  context: CanvasRenderingContext2D;
  x: number;
  y: number;
  spriteWidth: number;
  spriteHeight: number
}