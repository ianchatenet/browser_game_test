export type ImageData = {
  x: number;
  y: number;
  width: number;
  height: number;
}