import { ImageData } from './imageData';
import { SpriteI } from './sprite.interface';
import { SpriteDto } from './dtos/sprite.dto';
import { SpriteDrawDto } from './dtos/sprite.draw.dto';

export class Sprite implements SpriteI {

  private col: number;
  private row: number;
  private width: number;
  private height: number;
  private separator: number;
  private img: HTMLImageElement;

  constructor(dto: SpriteDto) {
    this.img = new Image();
    this.img.src = dto.sourceImg;
    this.col = dto.col;
    this.row = dto.row;
    this.width = dto.width;
    this.height = dto.height;
    this.separator = dto.separator;
  }

  draw(dto: SpriteDrawDto): void {
    const { context, x, y, spriteWidth, spriteHeight } = { ...dto };

    const imgData: ImageData = this.getSprite();
    context.imageSmoothingEnabled = false;
    context.drawImage(this.img, imgData.x, imgData.y, imgData.width, imgData.height, x, y, spriteWidth, spriteHeight);
  }

  private getSprite(): ImageData {
    const imageData: ImageData = {
      x: this.col * (this.width + this.separator),
      y: this.row * (this.height + this.separator),
      width: this.width,
      height: this.height
    };
    return imageData;
  }
}