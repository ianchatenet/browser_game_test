import { SpriteDrawDto } from './dtos/sprite.draw.dto';

export interface SpriteI {
  draw(dto: SpriteDrawDto): void;
}