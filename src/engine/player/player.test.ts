import { Player } from './player';
import { Enemy } from '../enemy/enemy';
import { Sprite } from '../sprite/sprite';
import { PlayerDto } from './dtos/player.dto';
import { WallLayer } from '../mapping/types/walls';
import { Wall } from '../../scripts/types.ts/wall';
import { PLAYER_DIRECTIONS, PLAYER_KEYS } from './player.enum';
import { EnemyConstructDto } from '../enemy/dto/construct.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';
import { EntityUpdateDto } from '../entity/dto/entity.update.dto';
import { EntityAttackAnimation, EntityMovingAnimation } from '../entity/dto/entity.dto';

describe('Player', () => {

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#update', () => {
    const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
    const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
    const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
    const playerDto: PlayerDto = {
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      downAttackAnimation: atkAnimation,
      leftAttackAnimation: atkAnimation,
      rightAttackAnimation: atkAnimation,
      upAttackAnimation: atkAnimation,
      speed: 1,
      damage: 1,
      health: 10
    };
    const player = new Player(playerDto);
    /* eslint-disable @typescript-eslint/no-explicit-any */
    const spyReplace = jest.spyOn(player as any, 'replace');
    const spyHandleMovements = jest.spyOn(player as any, 'handleMovements');
    const spyDraw = jest.spyOn(player as any, 'draw');
    const spyDrawSword = jest.spyOn(player as any, 'drawSword');

    it('Should update the entity for the first time, give all base data and call replace, handleMovements and draw', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      const dto: EntityUpdateDto = {
        frameCount: 0,
        tileData: {
          height: 16,
          width: 16,
          tilemapObjects: tilemapObjects
        }
      };

      spyReplace.mockReturnValueOnce({});
      spyHandleMovements.mockReturnValueOnce({});
      spyDraw.mockReturnValueOnce({});
      // act
      player.update(dto);
      // assert
      expect(spyReplace).toHaveBeenCalledTimes(1);
      expect(spyHandleMovements).toHaveBeenCalledTimes(1);
      expect(spyDraw).toHaveBeenCalledTimes(1);
      expect(player['width']).toStrictEqual(dto.tileData.width);
      expect(player['height']).toStrictEqual(dto.tileData.height);
      expect(player['tileWidth']).toStrictEqual(dto.tileData.width);
      expect(player['tileHeight']).toStrictEqual(dto.tileData.height);
      expect(player['velocity']).toStrictEqual(dto.tileData.height * player['speed'] / 100);
    });

    it('Should call replace, drawSword and draw', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };

      const dto: EntityUpdateDto = {
        frameCount: 0,
        tileData: {
          height: 16,
          width: 16,
          tilemapObjects: tilemapObjects
        }
      };

      player['isAttacking'] = true; // player is attacking so we will draw the sword
      spyReplace.mockReturnValueOnce({});
      spyDrawSword.mockReturnValueOnce({});
      spyDraw.mockReturnValueOnce({});
      // act
      player.update(dto);
      // assert
      expect(spyReplace).toHaveBeenCalledTimes(1);
      expect(spyDrawSword).toHaveBeenCalledTimes(1);
      expect(spyDraw).toHaveBeenCalledTimes(1);
      expect(player['width']).toStrictEqual(dto.tileData.width);
      expect(player['height']).toStrictEqual(dto.tileData.height);
      expect(player['tileWidth']).toStrictEqual(dto.tileData.width);
      expect(player['tileHeight']).toStrictEqual(dto.tileData.height);
      expect(player['velocity']).toStrictEqual(dto.tileData.height * player['speed'] / 100);
    });
  });

  describe('#handleMovements', () => {

    it('Should not move and call animate', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      player['velocity'] = 2;
      const baseY = player['y'];
      const baseX = player['x'];
      /* eslint-disable @typescript-eslint/no-explicit-any */
      const spyAnimate = jest.spyOn(player as any, 'animate');
      spyAnimate.mockReturnValueOnce({});
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      // act
      player.handleMovements(tilemapObjects, 0);
      // assert
      expect(spyAnimate).toHaveBeenCalledTimes(1);
      expect(player['y']).toStrictEqual(baseY);
      expect(player['x']).toStrictEqual(baseX);
    });

    it('Should move up', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      player['velocity'] = 2;
      const baseY = player['y'];
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ARROW_UP }));
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      // act
      player.handleMovements(tilemapObjects, 0);
      // assert
      expect(player['y']).toBeLessThan(baseY);
    });

    it('Should move down', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      player['velocity'] = 2;
      const baseY = player['y'];
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ARROW_DOWN }));
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      // act
      player.handleMovements(tilemapObjects, 0);
      // assert
      expect(player['y']).toBeGreaterThan(baseY);
    });

    it('Should move left', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      player['velocity'] = 2;
      const baseX = player['x'];
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ARROW_LEFT }));
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      // act
      player.handleMovements(tilemapObjects, 0);
      // assert
      expect(player['x']).toBeLessThan(baseX);
    });

    it('Should move right', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      player['velocity'] = 2;
      const baseX = player['x'];
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ARROW_RIGHT }));
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      // act
      player.handleMovements(tilemapObjects, 0);
      // assert
      expect(player['x']).toBeGreaterThan(baseX);
    });

    it('Should call handleAttack', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      const spyHandleAttack = jest.spyOn(player as any, 'handleAttack');
      player['velocity'] = 2;
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ATTACK }));
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      spyHandleAttack.mockReturnValueOnce({});
      // act
      player.handleMovements(tilemapObjects, 0);
      // assert
      expect(spyHandleAttack).toHaveBeenCalledTimes(1);
    });
  });

  describe('#listenInputs', () => {

    it('Should add 2 key and  remove 1', () => {
      // arrange
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const dto: PlayerDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        damage: 1,
        health: 10
      };
      const player = new Player(dto);
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ARROW_RIGHT }));
      document.dispatchEvent(new KeyboardEvent('keydown', { 'code': PLAYER_KEYS.ARROW_DOWN }));
      // act
      document.dispatchEvent(new KeyboardEvent('keyup', { 'code': PLAYER_KEYS.ARROW_RIGHT }));
      // assert
      expect(player['pressedKeys']).toStrictEqual([PLAYER_KEYS.ARROW_DOWN]);
    });
  });

  describe('#checkPosition', () => {
    const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
    const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
    const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
    const dto: PlayerDto = {
      speed: 1,
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      downAttackAnimation: atkAnimation,
      leftAttackAnimation: atkAnimation,
      rightAttackAnimation: atkAnimation,
      upAttackAnimation: atkAnimation,
      damage: 1,
      health: 10
    };
    const player = new Player(dto);

    it('Should return false because player is in a wall', () => {
      // arrange
      const baseHeight = 16;
      const baseWidth = 16;
      // get starting value
      player['_height'] = baseHeight; // set the height without calling update
      player['_width'] = baseWidth; // set the width without calling update
      player['tileHeight'] = baseHeight; // set the tileHeight without calling update
      player['tileWidth'] = baseWidth; // set the tileWidth without calling update

      const wallLayer: WallLayer = { col: 0, row: 0 };
      const wall: Wall = { x: wallLayer.col * player['tileWidth'], y: wallLayer.row * player['tileHeight'] };

      const tilemapObjects: TilemapObjects = {
        walls: [wall],
        enemies: [],
        player: player
      };

      // act
      const result: boolean = player['checkPosition'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(false);
    });


    it('Should return false because player is in a enemy', () => {
      // arrange
      const baseHeight = 16;
      const baseWidth = 16;
      // get starting value
      player['_height'] = baseHeight; // set the height without calling update
      player['_width'] = baseWidth; // set the width without calling update
      player['tileHeight'] = baseHeight; // set the tileHeight without calling update
      player['tileWidth'] = baseWidth; // set the tileWidth without calling update

      const idleSPrite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleSprites: Sprite[] = [idleSPrite, idleSPrite, idleSPrite];
      const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };

      const dto: EnemyConstructDto = {
        speed: 2,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        damage: 1,
        health: 10
      };

      const enemy = new Enemy(dto);
      enemy['_x'] = 0;
      enemy['_y'] = 0;
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [enemy],
        player: player
      };

      // act
      const result: boolean = player['checkPosition'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return true because player is not in a wall', () => {
      // arrange
      const baseHeight = 16;
      const baseWidth = 16;
      // get starting value
      player['_height'] = baseHeight; // set the height without calling update
      player['_width'] = baseWidth; // set the width without calling update
      player['tileHeight'] = baseHeight; // set the tileHeight without calling update
      player['tileWidth'] = baseWidth; // set the tileWidth without calling update

      const wallLayer: WallLayer = { col: 1, row: 1 };
      const wall: Wall = { x: wallLayer.col * player['tileWidth'], y: wallLayer.row * player['tileHeight'] };
      const tilemapObjects: TilemapObjects = {
        walls: [wall],
        enemies: [],
        player: player
      };
      // act
      const result: boolean = player['checkPosition'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(true);
    });
  });

  describe('#handleAttack', () => {
    const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
    const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
    const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
    const playerDto: PlayerDto = {
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      downAttackAnimation: atkAnimation,
      leftAttackAnimation: atkAnimation,
      rightAttackAnimation: atkAnimation,
      upAttackAnimation: atkAnimation,
      speed: 1,
      damage: 1,
      health: 10
    };
    const player = new Player(playerDto);
    const spyAnimateAttack = jest.spyOn(player as any, 'animateAttack');

    it('Should do nothing because the player do not have a direction', () => {
      // arrange
      // act
      player['handleAttack'](8);
      // assert
      expect(spyAnimateAttack).toHaveBeenCalledTimes(0);
    });

    it('Should attack with up direction', () => {
      // arrange
      player['direction'] = PLAYER_DIRECTIONS.UP;
      spyAnimateAttack.mockReturnValueOnce({});
      // act
      player['handleAttack'](8);
      // assert
      expect(spyAnimateAttack).toHaveBeenCalledTimes(1);
    });

    it('Should attack with down direction', () => {
      // arrange
      player['direction'] = PLAYER_DIRECTIONS.DOWN;
      spyAnimateAttack.mockReturnValueOnce({});
      // act
      player['handleAttack'](8);
      // assert
      expect(spyAnimateAttack).toHaveBeenCalledTimes(1);
    });

    it('Should attack with left direction', () => {
      // arrange
      player['direction'] = PLAYER_DIRECTIONS.LEFT;
      spyAnimateAttack.mockReturnValueOnce({});
      // act
      player['handleAttack'](8);
      // assert
      expect(spyAnimateAttack).toHaveBeenCalledTimes(1);
    });

    it('Should attack with right direction', () => {
      // arrange
      player['direction'] = PLAYER_DIRECTIONS.RIGHT;
      spyAnimateAttack.mockReturnValueOnce({});
      // act
      player['handleAttack'](8);
      // assert
      expect(spyAnimateAttack).toHaveBeenCalledTimes(1);
    });
  });

  describe('#animateAttack', () => {
    const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
    const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
    const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
    const playerDto: PlayerDto = {
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      downAttackAnimation: atkAnimation,
      leftAttackAnimation: atkAnimation,
      rightAttackAnimation: atkAnimation,
      upAttackAnimation: atkAnimation,
      speed: 1,
      damage: 1,
      health: 10
    };
    const player = new Player(playerDto);

    it('Should return an example with success', () => {
      // arrange
      player['lastFrameCount'] = 0;
      // act
      player['animateAttack'](8, atkAnimation);
      // assert
      expect(player['isAttacking']).toStrictEqual(true);
      expect(player['lastFrameCount']).toStrictEqual(8);
    });

  });

  describe('#drawSword', () => {
    const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
    const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
    const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
    const dto: PlayerDto = {
      speed: 1,
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      downAttackAnimation: atkAnimation,
      leftAttackAnimation: atkAnimation,
      rightAttackAnimation: atkAnimation,
      upAttackAnimation: atkAnimation,
      damage: 1,
      health: 10
    };
    const player = new Player(dto);
    const spyDrawSprite = jest.spyOn(idlePlayerSprite, 'draw');

    it('Should do nothing because no direction', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      player['currentAnimation'] = atkAnimation;
      // act
      player['drawSword'](8, tilemapObjects);
      // assert
      expect(spyDrawSprite).toHaveBeenCalledTimes(0);
    });

    it('Should draw sword when the player direction is UP', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      player['direction'] = PLAYER_DIRECTIONS.UP;
      player['currentAnimation'] = atkAnimation;
      spyDrawSprite.mockReturnValueOnce();
      // act
      player['drawSword'](8, tilemapObjects);
      // assert
      expect(spyDrawSprite).toHaveBeenCalledTimes(1);
    });

    it('Should draw sword when the player direction is DOWN', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      player['direction'] = PLAYER_DIRECTIONS.DOWN;
      player['currentAnimation'] = atkAnimation;
      spyDrawSprite.mockReturnValueOnce();
      // act
      player['drawSword'](8, tilemapObjects);
      // assert
      expect(spyDrawSprite).toHaveBeenCalledTimes(1);
    });

    it('Should draw sword when the player direction is LEFT', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      player['direction'] = PLAYER_DIRECTIONS.LEFT;
      player['currentAnimation'] = atkAnimation;
      spyDrawSprite.mockReturnValueOnce();
      // act
      player['drawSword'](8, tilemapObjects);
      // assert
      expect(spyDrawSprite).toHaveBeenCalledTimes(1);
    });

    it('Should draw sword when the player direction is RIGHT', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      player['direction'] = PLAYER_DIRECTIONS.RIGHT;
      player['currentAnimation'] = atkAnimation;
      spyDrawSprite.mockReturnValueOnce();
      // act
      player['drawSword'](8, tilemapObjects);
      // assert
      expect(spyDrawSprite).toHaveBeenCalledTimes(1);
    });
  });

  describe('#handleHitting', () => {
    const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
    const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
    const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
    const playerDto: PlayerDto = {
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      downAttackAnimation: atkAnimation,
      leftAttackAnimation: atkAnimation,
      rightAttackAnimation: atkAnimation,
      upAttackAnimation: atkAnimation,
      speed: 1,
      damage: 1,
      health: 10
    };

    const dto: EnemyConstructDto = {
      speed: 2,
      idleAnimation: idleAnimation,
      downMovingAnimation: idleAnimation,
      leftMovingAnimation: idleAnimation,
      rightMovingAnimation: idleAnimation,
      upMovingAnimation: idleAnimation,
      damage: 1,
      health: 10
    };


    const player = new Player(playerDto);
    it('Should do nothing because no enemy', () => {
      // arrange
      const enemy = new Enemy(dto);
      const spyGetDamage = jest.spyOn(enemy, 'getDamage');
      enemy['_x'] = 0;
      enemy['_y'] = 0;
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [enemy],
        player: player
      };
      // act
      player['handleHitting'](10, 10, tilemapObjects, 1);
      // assert
      expect(spyGetDamage).toHaveBeenCalledTimes(0);
    });

    it('Should do damage to enemy', () => {
      // arrange
      const enemy = new Enemy(dto);
      const spyGetDamage = jest.spyOn(enemy, 'getDamage');
      enemy['_x'] = 1, 5;
      enemy['_y'] = 1, 5;
      enemy['_width'] = 1;
      enemy['_height'] = 1;
      player['_width'] = 1;
      player['_height'] = 1;
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [enemy],
        player: player
      };
      // act
      player['handleHitting'](1, 1, tilemapObjects, 1);
      // assert
      expect(spyGetDamage).toHaveBeenCalledTimes(1);
    });
  });
});