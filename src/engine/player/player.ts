import { Enemy } from '../enemy/enemy';
import { Entity } from '../entity/entity';
import { PlayerDto } from './dtos/player.dto';
import { PLAYER_DIRECTIONS, PLAYER_KEYS } from './player.enum';
import { EntityAttackAnimation } from '../entity/dto/entity.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';
import { EntityUpdateDto } from '../entity/dto/entity.update.dto';

export class Player extends Entity {

  private damage: number;
  private health: number;

  private userKeys: PLAYER_KEYS[] = Object.values(PLAYER_KEYS);

  private pressedKeys: string[] = [];
  private direction: PLAYER_DIRECTIONS;

  protected isAttacking = false;


  constructor(dto: PlayerDto) {
    super(dto);
    this._x = 0;
    this._y = 100;
    this.health = dto.health;
    this.damage = dto.damage;
    this.listenInputs();
  }

  override update(dto: EntityUpdateDto): void {
    const { tileData, frameCount } = { ...dto };

    this.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
    this.replace(tileData);

    this._width = tileData.width;
    this._height = tileData.height;
    this.tileWidth = tileData.width;
    this.tileHeight = tileData.height;
    this.velocity = this.speed * this.width / 100;

    if (this.isAttacking === true) {
      this.drawSword(frameCount, tileData.tilemapObjects);
    } else {
      this.handleMovements(tileData.tilemapObjects, frameCount);
    }

    this.draw();
  }

  override handleMovements(tilemapObjects: TilemapObjects, frameCount: number) {
    if (this.pressedKeys.length < 1) return this.animate(frameCount, this.idleAnimation);

    switch (this.pressedKeys[0]) {
      case 'ArrowUp':
        this.handleMoveUp(frameCount, tilemapObjects);
        this.direction = PLAYER_DIRECTIONS.UP;
        break;
      case 'ArrowDown':
        this.handleMoveDown(frameCount, tilemapObjects);
        this.direction = PLAYER_DIRECTIONS.DOWN;
        break;
      case 'ArrowLeft':
        this.handleMoveLeft(frameCount, tilemapObjects);
        this.direction = PLAYER_DIRECTIONS.LEFT;
        break;
      case 'ArrowRight':
        this.handleMoveRight(frameCount, tilemapObjects);
        this.direction = PLAYER_DIRECTIONS.RIGHT;
        break;
      case 'KeyC':
        this.handleAttack(frameCount);
        break;
    }
  }

  private listenInputs() {
    document.addEventListener('keydown', (e) => {
      if (this.userKeys.includes(e.code as PLAYER_KEYS)) {
        const keyIndex: number = this.pressedKeys.findIndex((elmt) => elmt === e.code);
        if (keyIndex === -1) this.pressedKeys.unshift(e.code);
      }
    });

    document.addEventListener('keyup', (e) => {
      if (this.userKeys.includes(e.code as PLAYER_KEYS)) {
        const keyIndex: number = this.pressedKeys.findIndex((elmt) => elmt === e.code);
        if (keyIndex !== -1) this.pressedKeys.splice(keyIndex, 1);
      }
    });
  }

  private handleMoveRight(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkPosition(this._x + this.velocity, this._y, tilemapObjects)) this._x += this.velocity;
    this.animate(frameCount, this.rightMovingAnimation);
  }

  private handleMoveLeft(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkPosition(this._x - this.velocity, this._y, tilemapObjects)) this._x -= this.velocity;
    this.animate(frameCount, this.leftMovingAnimation);
  }

  private handleMoveUp(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkPosition(this._x, this._y - this.velocity, tilemapObjects)) this._y -= this.velocity;
    this.animate(frameCount, this.upMovingAnimation);
  }

  private handleMoveDown(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkPosition(this._x, this._y + this.velocity, tilemapObjects)) this._y += this.velocity;
    this.animate(frameCount, this.downMovingAnimation);
  }

  private checkPosition(x: number, y: number, tilemapObjects: TilemapObjects): boolean {
    for (let i = 0; i < tilemapObjects.walls.length; i++) {
      const wall = tilemapObjects.walls[i];

      const isInX = x + this.width > wall.x && x < wall.x + this.tileWidth;
      const isInY = y + this.height > wall.y && y < wall.y + this.tileHeight;
      if (isInX && isInY) return false;
    }

    for (let i = 0; i < tilemapObjects.enemies.length; i++) {
      const enemy = tilemapObjects.enemies[i];

      const isInX = x + this.width > enemy.x && x < enemy.x + this.tileWidth;
      const isInY = y + this.height > enemy.y && y < enemy.y + this.tileHeight;
      if (isInX && isInY) return false;
    }

    return true;
  }

  private handleAttack(frameCount: number): void {

    switch (this.direction) {
      case PLAYER_DIRECTIONS.UP:
        this.animateAttack(frameCount, this.upAttackAnimation);
        break;
      case PLAYER_DIRECTIONS.DOWN:
        this.animateAttack(frameCount, this.downAttackAnimation);
        break;
      case PLAYER_DIRECTIONS.LEFT:
        this.animateAttack(frameCount, this.leftAttackAnimation);
        break;
      case PLAYER_DIRECTIONS.RIGHT:
        this.animateAttack(frameCount, this.rightAttackAnimation);
        break;
      default:
        break;
    }
  }

  private animateAttack(frameCount: number, animation: EntityAttackAnimation) {
    if (animation !== undefined) {

      if (!animation.sprites.includes(this.currentSprite) || this.lastFrameCount <= frameCount - animation.frameLag) {
        this.currentAnimation = animation;
        this.currentSprite = animation.sprites[0];
        this.lastFrameCount = frameCount;
      }
    }
    this.isAttacking = true;
  }

  private drawSword(frameCount: number, tilemapObjects: TilemapObjects) {

    switch (this.direction) {
      case PLAYER_DIRECTIONS.UP: {
        const swordX: number = this._x;
        const swordY: number = this._y - this._height;
        this.currentAnimation.sprites[1].draw({ context: this.context, x: swordX, y: swordY, spriteWidth: this.width, spriteHeight: this.height });
        this.handleHitting(swordX, swordY, tilemapObjects, frameCount);
        if (frameCount >= this.lastFrameCount + this.currentAnimation.frameLag) this.isAttacking = false;
      }
        break;
      case PLAYER_DIRECTIONS.DOWN: {
        const swordX: number = this._x;
        const swordY: number = this._y + this.height;
        this.currentAnimation.sprites[1].draw({ context: this.context, x: swordX, y: swordY, spriteWidth: this.width, spriteHeight: this.height });
        this.handleHitting(swordX, swordY, tilemapObjects, frameCount);
        if (frameCount >= this.lastFrameCount + this.currentAnimation.frameLag) this.isAttacking = false;
      }
        break;
      case PLAYER_DIRECTIONS.LEFT: {
        const swordX: number = this._x - this.width;
        const swordY: number = this._y;
        this.currentAnimation.sprites[1].draw({ context: this.context, x: swordX, y: swordY, spriteWidth: this.width, spriteHeight: this.height });
        this.handleHitting(swordX, swordY, tilemapObjects, frameCount);
        if (frameCount >= this.lastFrameCount + this.currentAnimation.frameLag) this.isAttacking = false;
      }
        break;
      case PLAYER_DIRECTIONS.RIGHT: {
        const swordX: number = this._x + this.width;
        const swordY: number = this._y;
        this.currentAnimation.sprites[1].draw({ context: this.context, x: swordX, y: swordY, spriteWidth: this.width, spriteHeight: this.height });
        this.handleHitting(swordX, swordY, tilemapObjects, frameCount);
        if (frameCount >= this.lastFrameCount + this.currentAnimation.frameLag) this.isAttacking = false;
      }
        break;
      default:
        break;
    }
  }

  private handleHitting(x: number, y: number, tilemapObjects: TilemapObjects, frameCount: number) {
    for (let i = 0; i < tilemapObjects.enemies.length; i++) {
      const enemy: Enemy = tilemapObjects.enemies[i];
      const isInX: boolean = x + this._width > enemy.x && x < enemy.x + enemy.width;
      const isInY: boolean = y + this._height > enemy.y && y < enemy.y + enemy.height;
      if (isInX && isInY) {
        enemy.getDamage({ x, y }, this.damage, frameCount);
      }
    }
  }
}