import { EntityDto } from '../../entity/dto/entity.dto';

export type PlayerDto = Required<EntityDto> & {
  health: number;
  damage: number;
}