export enum PLAYER_KEYS {
  ARROW_DOWN = 'ArrowDown',
  ARROW_UP = 'ArrowUp',
  ARROW_RIGHT = 'ArrowRight',
  ARROW_LEFT = 'ArrowLeft',
  ATTACK = 'KeyC'
}

export enum PLAYER_DIRECTIONS {
  DOWN = 'DOWN',
  UP = 'UP',
  RIGHT = 'RIGHT',
  LEFT = 'LEFT'
}