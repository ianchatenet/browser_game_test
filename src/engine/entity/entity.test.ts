import { Entity } from './entity';
import { Sprite } from '../sprite/sprite';
import { Player } from '../player/player';
import { PlayerDto } from '../player/dtos/player.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';
import { EntityUpdateDto, TileData } from './dto/entity.update.dto';
import { EntityAttackAnimation, EntityDto, EntityMovingAnimation } from './dto/entity.dto';

/**
 * @jest-environment jsdom
 */

describe('Entity', () => {
  const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
  const idlePlayerSprite2 = new Sprite({ sourceImg: '', col: 0, row: 2, height: 16, width: 16, separator: 1 });
  const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite2], frameLag: 8 };
  const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
  const playerDto: PlayerDto = {
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    upMovingAnimation: idleAnimation,
    downAttackAnimation: atkAnimation,
    leftAttackAnimation: atkAnimation,
    rightAttackAnimation: atkAnimation,
    upAttackAnimation: atkAnimation,
    speed: 1,
    damage: 1,
    health: 10
  };
  const entityDto: EntityDto = { ...playerDto };
  const entity = new Entity(entityDto);
  const player = new Player(playerDto);
  describe('public method', () => {

    describe('#update', () => {
      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
      const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
      const playerDto: PlayerDto = {
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        downAttackAnimation: atkAnimation,
        leftAttackAnimation: atkAnimation,
        rightAttackAnimation: atkAnimation,
        upAttackAnimation: atkAnimation,
        speed: 1,
        damage: 1,
        health: 10
      };
      const entity = new Entity(playerDto);
      /* eslint-disable @typescript-eslint/no-explicit-any */
      const spyReplace = jest.spyOn(entity as any, 'replace');
      const spyHandleMovements = jest.spyOn(entity as any, 'handleMovements');
      const spyDraw = jest.spyOn(entity as any, 'draw');

      it('Should update the entity for the first time, give all base data', () => {
        // arrange
        const tilemapObjects: TilemapObjects = {
          walls: [],
          enemies: [],
          player: player
        };
        const dto: EntityUpdateDto = {
          frameCount: 0,
          tileData: {
            height: 16,
            width: 16,
            tilemapObjects: tilemapObjects
          }
        };

        spyReplace.mockReturnValueOnce({});
        spyHandleMovements.mockReturnValueOnce({});
        spyDraw.mockReturnValueOnce({});
        // act
        entity.update(dto);
        // assert
        expect(spyReplace).toHaveBeenCalledTimes(1);
        expect(spyHandleMovements).toHaveBeenCalledTimes(1);
        expect(spyDraw).toHaveBeenCalledTimes(1);
        expect(entity['width']).toStrictEqual(dto.tileData.width);
        expect(entity['height']).toStrictEqual(dto.tileData.height);
        expect(entity['tileWidth']).toStrictEqual(dto.tileData.width);
        expect(entity['tileHeight']).toStrictEqual(dto.tileData.height);
        expect(entity['velocity']).toStrictEqual(dto.tileData.height * entity['speed'] / 100);
      });
    });

  });

  describe('protected method', () => {

    describe('#handleMovements', () => {

      it('Should throw because not implemented in this class', () => {
        const tilemapObjects: TilemapObjects = {
          walls: [],
          enemies: [],
          player: player
        };
        expect(() => entity['handleMovements'](tilemapObjects, 0)).toThrow('Not implemented, implement it in child class !');
      });

    });

    describe('#animate', () => {

      const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
      const idlePlayerSprite2 = new Sprite({ sourceImg: '', col: 0, row: 2, height: 16, width: 16, separator: 1 });
      const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite2], frameLag: 8 };
      const dto: EntityDto = {
        speed: 1,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation
      };
      const entity = new Entity(dto);

      it('Should call animate and do nothing because not enough frame hav passed', () => {
        // arrange
        //act
        entity['animate'](1, idleAnimation);
        // assert
        expect(entity['currentSprite']).toStrictEqual(idlePlayerSprite);
      });

      it('Should call animate and pass to next sprite', () => {
        // arrange
        //act
        entity['animate'](8, idleAnimation);
        // assert
        expect(entity['currentSprite']).not.toStrictEqual(idlePlayerSprite);
        expect(entity['currentSprite']).toStrictEqual(idlePlayerSprite2);
      });

      it('Should call animate and return to first sprite', () => {
        // arrange
        //act
        entity['animate'](16, idleAnimation);
        // assert
        expect(entity['currentSprite']).toStrictEqual(idlePlayerSprite);
      });
    });
  });

  describe('private method', () => {

    describe('#draw', () => {
      const spyDrawSprite = jest.spyOn(idlePlayerSprite, 'draw');

      it('Should draw the entity', () => {
        // arrange
        spyDrawSprite.mockReturnValueOnce();
        // act
        entity['draw']();
        // assert
        expect(spyDrawSprite).toHaveBeenCalledTimes(1);
      });
    });

    describe('#replace', () => {

      it('Should replace the player on resize', () => {
        // arrange
        const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
        const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
        const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
        const playerDto: PlayerDto = {
          idleAnimation: idleAnimation,
          downMovingAnimation: idleAnimation,
          leftMovingAnimation: idleAnimation,
          rightMovingAnimation: idleAnimation,
          upMovingAnimation: idleAnimation,
          downAttackAnimation: atkAnimation,
          leftAttackAnimation: atkAnimation,
          rightAttackAnimation: atkAnimation,
          upAttackAnimation: atkAnimation,
          speed: 1,
          damage: 1,
          health: 10
        };
        const entity = new Entity(playerDto);

        const tilemapObjects: TilemapObjects = {
          walls: [],
          enemies: [],
          player: player
        };

        const tileData: TileData = {
          height: 32,
          width: 32,
          tilemapObjects: tilemapObjects
        };

        const baseHeight = 16;
        const baseWidth = 16;
        // get starting value
        entity['_height'] = baseHeight; // set the height without calling update
        entity['_width'] = baseWidth; // set the width without calling update
        entity['tileHeight'] = baseHeight; // set the tileHeight without calling update
        entity['tileWidth'] = baseWidth; // set the tileWidth without calling update
        const baseX = entity['x'];
        const baseY = entity['y'];
        // act

        entity['replace'](tileData); // call the replace method
        // assert
        expect(entity['x']).toStrictEqual(baseX / baseWidth * tileData.width);
        expect(entity['y']).toStrictEqual(baseY / baseHeight * tileData.height);
      });
    });
  });
});