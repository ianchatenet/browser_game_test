import { SpriteI } from '../sprite/sprite.interface';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';
import { EntityUpdateDto, TileData } from './dto/entity.update.dto';
import { EntityMovingAnimation, EntityDto, EntityAttackAnimation } from './dto/entity.dto';

export class Entity {

  protected canvas: HTMLCanvasElement;
  protected context: CanvasRenderingContext2D;
  /**
   * used to position on tilemap for the firstRender
   */
  protected colX: number;
  /**
 * used to position on tilemap for the firstRender
 */
  protected colY: number;

  protected _x: number;
  protected _y: number;
  protected _height: number;
  protected _width: number;

  /**
  * @description Number of tile per frame
  */
  protected speed: number;

  /**
  * @description Entity speed by tile width
  */
  protected velocity: number;

  protected idleAnimation: EntityMovingAnimation;
  protected upMovingAnimation: EntityMovingAnimation;
  protected downMovingAnimation: EntityMovingAnimation;
  protected leftMovingAnimation: EntityMovingAnimation;
  protected rightMovingAnimation: EntityMovingAnimation;
  protected upAttackAnimation: EntityAttackAnimation;
  protected downAttackAnimation: EntityAttackAnimation;
  protected leftAttackAnimation: EntityAttackAnimation;
  protected rightAttackAnimation: EntityAttackAnimation;


  protected currentAnimation: EntityMovingAnimation;
  protected currentSprite: SpriteI;

  protected tileWidth: number;
  protected tileHeight: number;

  protected lastFrameCount: number;

  protected invincibilityFrame = 8;
  /**
   * framecount when the enemy got damaged
   */
  protected lastDamaged: number;

  private blink = 0;
  constructor(dto: EntityDto) {
    this.speed = dto.speed;
    this.idleAnimation = dto.idleAnimation;
    this.upMovingAnimation = dto.upMovingAnimation;
    this.downMovingAnimation = dto.downMovingAnimation;
    this.leftMovingAnimation = dto.leftMovingAnimation;
    this.rightMovingAnimation = dto.rightMovingAnimation;
    this.upAttackAnimation = dto.upAttackAnimation;
    this.downAttackAnimation = dto.downAttackAnimation;
    this.leftAttackAnimation = dto.leftAttackAnimation;
    this.rightAttackAnimation = dto.rightAttackAnimation;
    this.lastFrameCount = 0;
    this.currentAnimation = this.idleAnimation;
    this.currentSprite = this.idleAnimation.sprites[0];

    this.canvas = document.createElement('canvas');
    this.canvas.style.zIndex = '100';
    document.body.appendChild(this.canvas);
    this.context = this.canvas.getContext('2d');
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
  }

  setInitialPosition(colX: number, colY: number): void {
    this.colX = colX;
    this.colY = colY;
  }

  update(dto: EntityUpdateDto): void {
    const { tileData, frameCount } = { ...dto };

    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.replace(tileData);

    this._width = tileData.width;
    this._height = tileData.height;
    this.tileWidth = tileData.width;
    this.tileHeight = tileData.height;
    this.velocity = this.speed * this._width / 100;

    if (this._x === undefined) this._x = this.colX * this._width;
    if (this._y === undefined) this._y = this.colY * this.height;

    /**
     * make the entity blink if it get damaged
     */
    if (this.lastDamaged + this.invincibilityFrame > frameCount) {
      this.blink++;
      if (this.blink % 2 === 0) this.draw();
    } else {
      this.draw();
    }

    this.handleMovements(tileData.tilemapObjects, frameCount);
  }

  public get x(): number {
    return this._x;
  }

  public get y(): number {
    return this._y;
  }

  public get width(): number {
    return this._width;
  }

  public get height(): number {
    return this._height;
  }

  /* eslint-disable @typescript-eslint/no-unused-vars*/
  protected handleMovements(tilemapObjects: TilemapObjects, frameCount: number) {
    throw ('Not implemented, implement it in child class !');
  }

  protected animate(frameCount: number, animation: EntityMovingAnimation) {

    if (animation !== undefined) {

      if (!animation.sprites.includes(this.currentSprite) || this.lastFrameCount <= frameCount - animation.frameLag) {
        let spriteIndex: number = animation.sprites.findIndex((elmt: SpriteI) => elmt === this.currentSprite);
        if (spriteIndex >= animation.sprites.length - 1) spriteIndex = 0;
        else spriteIndex++;
        this.currentAnimation = animation;
        this.currentSprite = animation.sprites[spriteIndex];
        this.lastFrameCount = frameCount;
      }
    }
  }

  protected draw() {
    this.currentSprite.draw({ context: this.context, x: this.x, y: this.y, spriteWidth: this._width, spriteHeight: this.height });
  }

  /**
   * replace player when the window has resized
   */
  protected replace(tileData: TileData) {

    if (this._height && this._height !== tileData.height) {
      this._y = this._y / this.tileHeight * tileData.height;
    }

    if (this._width && this._width !== tileData.width) {
      this._x = this._x / this.tileWidth * tileData.width;
    }
  }
}