import { TilemapObjects } from '../../mapping/tilemap/tilemap.types';

export type EntityUpdateDto = {
  tileData: TileData;
  frameCount: number;
}

export type TileData = {
  width: number;
  height: number;
  tilemapObjects: TilemapObjects;
}