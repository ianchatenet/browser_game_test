import { SpriteI } from '../../sprite/sprite.interface';

export type EntityDto = {
  speed: number,
  idleAnimation: EntityMovingAnimation;
  leftMovingAnimation: EntityMovingAnimation;
  rightMovingAnimation: EntityMovingAnimation;
  upMovingAnimation: EntityMovingAnimation;
  downMovingAnimation: EntityMovingAnimation;
  leftAttackAnimation?: EntityAttackAnimation;
  rightAttackAnimation?: EntityAttackAnimation;
  upAttackAnimation?: EntityAttackAnimation;
  downAttackAnimation?: EntityAttackAnimation;
}

export type EntityMovingAnimation = {
  sprites: SpriteI[];
  frameLag: number;
}

export type EntityAttackAnimation = {
  sprites: [SpriteI, SpriteI];
  frameLag: number;
}