import { Tile } from './tile';
import { TileI } from './tile.interface';
import { Sprite } from '../../sprite/sprite';
import { TileDrawDto } from './tile.draw.dto';
import { SpriteI } from '../../sprite/sprite.interface';
import { SpriteDrawDto } from '../../sprite/dtos/sprite.draw.dto';

/**
 * @jest-environment jsdom
 */

describe('Tile', () => {
  const sprite: SpriteI = new Sprite({ sourceImg: '', col: 0, row: 0, width: 16, height: 16, separator: 1 });
  const tile: TileI = new Tile(sprite);
  const canvas: HTMLCanvasElement = global.document.createElement('canvas');
  /* eslint-disable @typescript-eslint/no-non-null-assertion */

  const ctx = canvas.getContext('2d')!;

  const spySpriteDraw = jest.spyOn(sprite, 'draw');

  describe('#draw', () => {
    it('Should call draw from sprite', () => {
      // arrange
      const dto: TileDrawDto = { context: ctx, x: 0, y: 0, width: 16, height: 16 };
      const expected: SpriteDrawDto = { context: dto.context, x: dto.x, y: dto.y, spriteWidth: dto.width, spriteHeight: dto.height };
      spySpriteDraw.mockReturnValueOnce();
      // act
      tile.draw(dto);
      // assert
      expect(spySpriteDraw).toHaveBeenCalledTimes(1);
      expect(spySpriteDraw).toHaveBeenCalledWith(expected);
    });
  });
});