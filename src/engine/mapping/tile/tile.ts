import { TileI } from './tile.interface';
import { TileDrawDto } from './tile.draw.dto';
import { SpriteI } from '../../sprite/sprite.interface';

export class Tile implements TileI {
  private sprite: SpriteI;

  private _x: number;
  private _y: number;

  constructor(sprite: SpriteI) {
    this.sprite = sprite;
  }

  draw(dto: TileDrawDto): void {
    const { context, x, y, width, height } = { ...dto };
    this.sprite.draw({ context: context, x: x, y: y, spriteWidth: width, spriteHeight: height });
    this._x = x;
    this._y = y;
  }

  public get x(): number {
    return this._x;
  }

  public get y(): number {
    return this._y;
  }
}