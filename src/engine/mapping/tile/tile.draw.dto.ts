export type TileDrawDto = {
  context: CanvasRenderingContext2D;
  x: number;
  y: number;
  width: number;
  height: number;
}