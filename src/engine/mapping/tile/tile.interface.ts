import { TileDrawDto } from './tile.draw.dto';

export interface TileI {
  draw(dto: TileDrawDto): void;
}