import { Tile } from '../tile/tile';
import { FloorI } from './floor.interface';
import { SpriteI } from '../../sprite/sprite.interface';

export class Floor extends Tile implements FloorI {
  constructor(sprite: SpriteI) {
    super(sprite);
  }
}