import { Enemy } from '../../enemy/enemy';

export type EnemyTilemap = {
  entity: Enemy,
  basePosition: { colX: number, colY: number }
}