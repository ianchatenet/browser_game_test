import { TileI } from '../tile/tile.interface';

export type Layer = Array<TileI[]>;


// need some test to specify layer widht and lenght
// type Test<T extends number> = Array<FixedLengthArray<TileI, T>>;

// type ArrayLengthMutationKeys = 'splice' | 'push' | 'pop' | 'shift' | 'unshift'
// type FixedLengthArray<T, L extends number, TObj = [T, ...Array<T>]> =
//   Pick<TObj, Exclude<keyof TObj, ArrayLengthMutationKeys>>
//   & {
//     readonly length: L
//     [I: number]: T
//     [Symbol.iterator]: () => IterableIterator<T>
//   }