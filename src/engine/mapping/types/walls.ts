/**
 * Describe the position in the layer
 */
export type WallLayer = {
  col: number;
  row: number
};