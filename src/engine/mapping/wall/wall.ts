import { Tile } from '../tile/tile';
import { WallI } from './wall.interface';
import { SpriteI } from '../../sprite/sprite.interface';

export class Wall extends Tile implements WallI {

  constructor(sprite: SpriteI) {
    super(sprite);
  }
}