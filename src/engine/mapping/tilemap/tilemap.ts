import { Wall } from '../wall/wall';
import { Layer } from '../types/layer';
import { EnemyTilemap } from '../types/enemy';
import { TileI } from '../tile/tile.interface';
import { TileDrawDto } from '../tile/tile.draw.dto';
import { TilemapObjects, WallTilemap } from './tilemap.types';
import { EntityUpdateDto } from '../../entity/dto/entity.update.dto';
import { TilemapDrawDto, TilesDrawDto } from './dto/tilemap.draw.dto';

export class Tilemap {

  name: string;
  private layers: Layer[];
  private enemies: EnemyTilemap[];
  private canvasList: CanvasRenderingContext2D[] = [];
  private drawn = false;
  private walls: WallTilemap[] = [];

  private oldWidth = 0;
  private oldHeight = 0;

  constructor(name: string, layers: Layer[], enemies: EnemyTilemap[] = []) {
    this.name = name;
    this.layers = layers;
    this.enemies = enemies;
    for (let i = 0; i < layers.length; i++) {
      const canvasLayer: HTMLCanvasElement = document.createElement('canvas');
      canvasLayer.id = 'layer' + i.toString();
      canvasLayer.width = window.innerWidth;
      canvasLayer.height = window.innerHeight;
      canvasLayer.style.zIndex = ((this.layers.length - i) * -1).toString();
      document.body.appendChild(canvasLayer);
      const context: CanvasRenderingContext2D = canvasLayer.getContext('2d');
      this.canvasList.push(context);
    }
  }

  draw(dto: TilemapDrawDto): TilemapObjects {

    if (this.enemies !== undefined) this.drawEnemies(dto);

    const drawTilesDto: TilesDrawDto = { height: dto.height, width: dto.width };
    this.drawTiles(drawTilesDto);

    const enemies = this.enemies.map((enemy) => enemy.entity);
    return { walls: this.walls, enemies: enemies, player: dto.player };
  }

  getRowCount(): number {
    return this.layers[0][0].length;
  }

  getColCount(): number {
    return this.layers[0].length;
  }

  private drawTiles(dto: TilesDrawDto) {
    const { height, width } = { ...dto };

    for (let i = 0; i < this.layers.length; i++) {
      const layer: Layer = this.layers[i];
      const context = this.canvasList[i];
      context.imageSmoothingEnabled = false;

      const isResized = context.canvas.width !== this.oldWidth || context.canvas.height !== this.oldHeight;
      const shouldUpdate = this.drawn === false || isResized;

      if (shouldUpdate === true) {
        for (let y = 0; y < layer.length; y++) {
          const row: TileI[] = layer[y];

          for (let x = 0; x < row.length; x++) {
            const tile: TileI = row[x];
            const dto: TileDrawDto = {
              context: context,
              height: height,
              width: width,
              x: x * width, // position by tile width to position x in the window
              y: y * height // position by tile width to position y in the window
            };

            if (tile) tile.draw(dto);
            if (tile instanceof Wall) this.walls.push({ x: tile.x, y: tile.y });
          }
        }

        if (i === this.layers.length - 1) {
          this.oldWidth = context.canvas.width;
          this.oldHeight = context.canvas.height;
        }
      }
    }

    this.drawn = true;
  }

  private drawEnemies(dto: TilemapDrawDto) {

    const { height, width, frameCount } = { ...dto };
    for (let i = 0; i < this.enemies.length; i++) {
      const enemy: EnemyTilemap = this.enemies[i];

      if (enemy.entity.health <= 0) {
        this.enemies.splice(i, 1);
        --i;
        continue;
      }

      enemy.entity.setInitialPosition(enemy.basePosition.colX, enemy.basePosition.colY);

      const enemies = this.enemies.map((elmt) => elmt.entity).filter((elmt) => enemy.entity !== elmt);
      const tilemapObjects: TilemapObjects = { walls: this.walls, enemies: enemies, player: dto.player };

      const updateEnemyDto: EntityUpdateDto = {
        frameCount: frameCount,
        tileData: {
          height: height,
          width: width,
          tilemapObjects: tilemapObjects
        }
      };

      enemy.entity.update(updateEnemyDto);
    }
  }
}