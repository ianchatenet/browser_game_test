import { Enemy } from '../../enemy/enemy';
import { Player } from '../../player/player';

export type WallTilemap = { x: number, y: number };
export type TilemapObjects = { walls: WallTilemap[], enemies: Enemy[], player: Player };