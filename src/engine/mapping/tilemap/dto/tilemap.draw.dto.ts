import { Player } from '../../../player/player';

export type TilemapDrawDto = {
  width: number;
  height: number;
  frameCount: number;
  player: Player;
}

export type TilesDrawDto = {
  width: number;
  height: number;
}