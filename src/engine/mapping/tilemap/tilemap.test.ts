import { Tilemap } from './tilemap';
import { Wall } from '../wall/wall';
import { Layer } from '../types/layer';
import { Enemy } from '../../enemy/enemy';
import { Sprite } from '../../sprite/sprite';
import { Player } from '../../player/player';
import { EnemyTilemap } from '../types/enemy';
import { TilemapObjects } from './tilemap.types';
import { TilemapDrawDto } from './dto/tilemap.draw.dto';
import { PlayerDto } from '../../player/dtos/player.dto';
import { EnemyConstructDto } from '../../enemy/dto/construct.dto';
import { EntityAttackAnimation, EntityMovingAnimation } from '../../entity/dto/entity.dto';

describe('Tilemap', () => {
  const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
  const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
  const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
  const dtoPlayer: PlayerDto = {
    speed: 1,
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    downAttackAnimation: atkAnimation,
    leftAttackAnimation: atkAnimation,
    rightAttackAnimation: atkAnimation,
    upAttackAnimation: atkAnimation,
    upMovingAnimation: idleAnimation,
    damage: 1,
    health: 10
  };
  const player = new Player(dtoPlayer);

  describe('#draw', () => {
    it('Should draw a tilemap', () => {
      // arrange
      const tilemap = new Tilemap('test', []);
      const dto: TilemapDrawDto = {
        height: 16,
        width: 16,
        frameCount: 0,
        player: player
      };
      // act
      const result: TilemapObjects = tilemap.draw(dto);
      // assert
      expect(result.walls.length).toStrictEqual(0);
    });

    it('Should draw a tilemap and all enemies', () => {
      // arrange
      const idleEnemySprite0 = new Sprite({ sourceImg: '', col: 0, row: 2, height: 16, width: 16, separator: 0 });
      const idleSprites: Sprite[] = [idleEnemySprite0];
      const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };
      const dtoEnemy: EnemyConstructDto = {
        speed: 2,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        damage: 1,
        health: 10
      };
      const enemy1 = new Enemy(dtoEnemy);
      const enemiesTilemap: EnemyTilemap[] = [
        {
          entity: enemy1,
          basePosition: { colX: 7, colY: 8 }
        }
      ];

      const spyDrawEn = jest.spyOn(enemy1, 'update');

      const tilemap = new Tilemap('test', [], enemiesTilemap);
      const dto: TilemapDrawDto = {
        height: 16,
        width: 16,
        frameCount: 0,
        player: player
      };
      spyDrawEn.mockReturnValueOnce();
      spyDrawEn.mockReturnValueOnce();
      // act
      // should get the same canvas context for same enemy entity
      const result1 = tilemap.draw(dto);
      const result2 = tilemap.draw(dto);
      // assert
      expect(result1.walls.length).toStrictEqual(0);
      expect(result1.enemies.length).toStrictEqual(1);
      expect(result1).toStrictEqual(result2);
    });

    it('Should return the tilemap objects', () => {
      // arrange
      const sprite = new Sprite({ sourceImg: '', col: 0, row: 0, height: 16, width: 16, separator: 0 });
      const wall = new Wall(sprite);
      const layer: Layer = [[wall]];
      const tilemap = new Tilemap('test', [layer]);
      const dto: TilemapDrawDto = {
        height: 16,
        width: 16,
        frameCount: 0,
        player: player
      };
      const spyDraw = jest.spyOn(wall, 'draw');
      spyDraw.mockReturnValue();
      // act
      const result: TilemapObjects = tilemap.draw(dto);
      // assert
      expect(result.walls.length).toStrictEqual(1);
    });


    it('Should draw a tilemap and all enemies', () => {
      // arrange
      const idleEnemySprite0 = new Sprite({ sourceImg: '', col: 0, row: 2, height: 16, width: 16, separator: 0 });
      const idleSprites: Sprite[] = [idleEnemySprite0];
      const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };
      const dtoEnemy: EnemyConstructDto = {
        speed: 2,
        idleAnimation: idleAnimation,
        downMovingAnimation: idleAnimation,
        leftMovingAnimation: idleAnimation,
        rightMovingAnimation: idleAnimation,
        upMovingAnimation: idleAnimation,
        damage: 1,
        health: 10
      };
      const enemy1 = new Enemy(dtoEnemy);
      enemy1['health'] = 0;
      const enemiesTilemap: EnemyTilemap[] = [
        {
          entity: enemy1,
          basePosition: { colX: 7, colY: 8 }
        }
      ];

      const spyDrawEn = jest.spyOn(enemy1, 'update');

      const tilemap = new Tilemap('test', [], enemiesTilemap);
      const dto: TilemapDrawDto = {
        height: 16,
        width: 16,
        frameCount: 0,
        player: player
      };
      // act
      // should get the same canvas context for same enemy entity
      const result1 = tilemap.draw(dto);
      // assert
      expect(result1.walls.length).toStrictEqual(0);
      expect(result1.enemies.length).toStrictEqual(0);
      expect(spyDrawEn).toHaveBeenCalledTimes(0);
    });
  });

});