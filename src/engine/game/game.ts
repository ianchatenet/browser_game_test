import { Player } from '../player/player';
import { PressedKeys } from './pressedKeys';
import { Tilemap } from '../mapping/tilemap/tilemap';
import { EntityUpdateDto } from '../entity/dto/entity.update.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';
import { TilemapDrawDto } from '../mapping/tilemap/dto/tilemap.draw.dto';

export class Game {
  private player: Player;

  private canvas: HTMLCanvasElement;
  private context: CanvasRenderingContext2D;

  private frameCount: number;
  private lastTime: number;
  private fps: number;

  private pressedKeys: PressedKeys;

  private xTiles: number;
  private yTiles: number;

  private tileWidth: number;
  private tileHeight: number;
  private tilemaps: Tilemap[];

  constructor(player: Player, tilemaps: Tilemap[]) {
    this.canvas = document.getElementById('main') as HTMLCanvasElement;

    this.lastTime = new Date().getTime();
    this.player = player;
    this.pressedKeys = {};
    this.tilemaps = tilemaps;
    this.frameCount = 0;
  }

  init() {
    this.resize();
    document.addEventListener('keydown', (e) => this.pressedKeys[e.code] = true);
    document.addEventListener('keyup', (e) => delete this.pressedKeys[e.code]);
    window.addEventListener('resize', () => this.resize());
    window.requestAnimationFrame(() => this.update());
    this.context = this.canvas.getContext('2d');
    this.context.imageSmoothingEnabled = false;
  }

  private update() {

    this.frameCount++;

    const currentTilemap: Tilemap = this.tilemaps[0];
    this.xTiles = currentTilemap.getRowCount();
    this.yTiles = currentTilemap.getColCount();
    
    this.tileWidth = Math.round(this.canvas.width / this.xTiles);
    this.tileHeight = Math.round(this.canvas.height / this.yTiles);

    this.calculateFps();
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

    const tilemapDto: TilemapDrawDto = {
      height: this.tileHeight,
      width: this.tileWidth,
      frameCount: this.frameCount,
      player: this.player
    };

    const tilemapObjects: TilemapObjects = currentTilemap.draw(tilemapDto);

    const playerUpdateDto: EntityUpdateDto = {
      frameCount: this.frameCount,
      tileData: {
        height: this.tileHeight,
        width: this.tileWidth,
        tilemapObjects: tilemapObjects
      }
    };

    this.player.update(playerUpdateDto);

    this.handleKeys(this.pressedKeys);
    window.requestAnimationFrame(() => this.update());
  }

  private calculateFps() {
    const currentTime = new Date().getTime();
    this.fps = Math.round(1000 / (currentTime - this.lastTime));
    this.lastTime = currentTime;
  }

  private handleKeys(pressedKeys: PressedKeys) {
    for (const key in pressedKeys) {
      if (key === 'KeyF') this.showFps();
    }
  }

  private showFps() {
    this.context.fillStyle = 'black';
    this.context.fillRect(15, 0, 18, 16);
    this.context.font = '16px Arial';
    this.context.fillStyle = 'red';
    this.context.fillText(this.fps.toString(), 16, 16);
  }

  private resize() {
    const canvasList: HTMLCollectionOf<HTMLCanvasElement> = document.body.getElementsByTagName('canvas');

    for (let i = 0; i < canvasList.length; i++) {
      const canvas: HTMLCanvasElement = canvasList[i];
      if (window.innerWidth !== canvas.width) canvas.width = window.innerWidth;
      if (window.innerHeight !== canvas.height) canvas.height = window.innerHeight;
    }
  }

}