import { Game } from './game';
import { Sprite } from '../sprite/sprite';
import { Player } from '../player/player';
import { Tile } from '../mapping/tile/tile';
import { Tilemap } from '../mapping/tilemap/tilemap';
import { PlayerDto } from '../player/dtos/player.dto';
import { TileI } from '../mapping/tile/tile.interface';
import { EntityMovingAnimation, EntityAttackAnimation } from '../entity/dto/entity.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';

/**
 * @jest-environment jsdom
 */
describe('Game', () => {
  const idlePlayerSprite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
  const idleAnimation: EntityMovingAnimation = { sprites: [idlePlayerSprite], frameLag: 8 };
  const atkAnimation: EntityAttackAnimation = { sprites: [idlePlayerSprite, idlePlayerSprite], frameLag: 8 };
  const playerDto: PlayerDto = {
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    upMovingAnimation: idleAnimation,
    downAttackAnimation: atkAnimation,
    leftAttackAnimation: atkAnimation,
    rightAttackAnimation: atkAnimation,
    upAttackAnimation: atkAnimation,
    speed: 1,
    health: 10,
    damage: 1
  };
  const player = new Player(playerDto);
  const canvas: HTMLCanvasElement = global.document.createElement('canvas');
  /* eslint-disable @typescript-eslint/no-non-null-assertion */

  const context: CanvasRenderingContext2D = canvas.getContext('2d')!;
  let game: Game;

  const spyPlayerUpdate = jest.spyOn(player, 'update');

  const sprite1 = new Sprite({ sourceImg: 'test', col: 0, row: 0, height: 16, width: 16, separator: 0 });
  const tile1 = new Tile(sprite1);
  const layer1: Array<TileI[]> = [[tile1, tile1]];

  const tilemap1 = new Tilemap('test', [layer1]);
  const tilemaps: Tilemap[] = [tilemap1];

  const spyTilemapDraw = jest.spyOn(tilemap1, 'draw');

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#constructor', () => {

    it('Should get the canvas', () => {
      // arrange
      const spyGetElementById = jest.spyOn(document, 'getElementById');
      const spyGetContext = jest.spyOn(canvas, 'getContext');
      spyGetContext.mockReturnValueOnce(context);
      spyGetElementById.mockReturnValueOnce(canvas);
      // act
      game = new Game(player, tilemaps);
      // assert
      expect(spyGetElementById).toHaveBeenNthCalledWith(1, 'main');
    });
  });

  describe('#init', () => {
    it('Should add event keydown and keyup and call requestAnimationFrame', () => {
      // arrange
      const spyDoc = jest.spyOn(document, 'addEventListener');
      const spyWindow = jest.spyOn(window, 'addEventListener');
      const requestAnimationFrame = jest.spyOn(window, 'requestAnimationFrame');
      // act
      game.init();
      // assert
      expect(spyDoc).toHaveBeenCalledTimes(2);
      expect(spyWindow).toHaveBeenCalledTimes(1);
      expect(requestAnimationFrame).toHaveBeenCalledTimes(1);
    });
  });

  describe('#update', () => {
    it('Should clear the canvas, call player.update', () => {
      // arrange
      const spyClear = jest.spyOn(context, 'clearRect');
      const spyDraw = jest.spyOn(context, 'drawImage');
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        walls: [],
        player: player
      };
      spyDraw.mockReturnValue();
      spyPlayerUpdate.mockReturnValueOnce();
      spyTilemapDraw.mockReturnValue(tilemapObjects);
      // act
      game['update']();
      // assert
      expect(spyClear).toHaveBeenCalledTimes(1);
      expect(spyPlayerUpdate).toHaveBeenCalledTimes(1);
    });

    it('Should call showFps if keyF is pressed', () => {
      // arrange
      const spyClear = jest.spyOn(context, 'clearRect');
      const spyFillText = jest.spyOn(context, 'fillText');
      const spyDraw = jest.spyOn(context, 'drawImage');
      game['pressedKeys'] = { KeyF: true };

      const tilemapObjects: TilemapObjects = {
        enemies: [],
        walls: [],
        player: player
      };
      spyPlayerUpdate.mockReturnValueOnce();
      spyDraw.mockReturnValue();
      spyTilemapDraw.mockReturnValue(tilemapObjects);
      // act
      game['update']();
      // assert
      expect(spyClear).toHaveBeenCalledTimes(1);
      expect(spyPlayerUpdate).toHaveBeenCalledTimes(1);
      expect(spyFillText).toHaveBeenCalledTimes(1);
    });
  });
});