export type PressedKeys = {
  [key: string]: boolean;
}