import { Enemy } from './enemy';
import { Sprite } from '../sprite/sprite';
import { Player } from '../player/player';
import { PlayerDto } from '../player/dtos/player.dto';
import { EnemyConstructDto } from './dto/construct.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';
import { EntityAttackAnimation, EntityMovingAnimation } from '../entity/dto/entity.dto';

describe('Enemy', () => {

  const idleSPrite = new Sprite({ sourceImg: '', col: 0, row: 1, height: 16, width: 16, separator: 1 });
  const idleSprites: Sprite[] = [idleSPrite, idleSPrite, idleSPrite];
  const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };
  const atkAnimation: EntityAttackAnimation = { sprites: [idleSPrite, idleSPrite], frameLag: 8 };
  const playerDto: PlayerDto = {
    speed: 1,
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    upMovingAnimation: idleAnimation,
    downAttackAnimation: atkAnimation,
    leftAttackAnimation: atkAnimation,
    rightAttackAnimation: atkAnimation,
    upAttackAnimation: atkAnimation,
    damage: 1,
    health: 10
  };
  const player = new Player(playerDto);

  const dto: EnemyConstructDto = {
    speed: 2,
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    upMovingAnimation: idleAnimation,
    damage: 1,
    health: 10
  };

  const enemy = new Enemy(dto);
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const spyAnimate = jest.spyOn(enemy as any, 'animate');

  describe('#handleMovements', () => {
    it('Should call animate', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        walls: [],
        enemies: [],
        player: player
      };
      // act
      enemy.handleMovements(tilemapObjects, 1);
      // assert
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });
  });

  describe('#getDamage', () => {

    it('Should lower the helth point and do nothing else', () => {
      // arrange
      // act
      enemy.getDamage({ x: 1, y: 1 },1, 1);
      // assert
      expect(enemy['health']).toStrictEqual(9);
    });

    it('Should clear and remove the canvas', () => {
      // arrange
      const spyClearRect = jest.spyOn(enemy['context'], 'clearRect');
      const spyRemove = jest.spyOn(enemy['canvas'], 'remove');
      spyClearRect.mockReturnValueOnce();
      spyRemove.mockReturnValueOnce();
      // act
      enemy.getDamage({ x: 1, y: 1 }, 100, 100);
      // assert
      expect(spyClearRect).toHaveBeenCalledTimes(1);
      expect(spyRemove).toHaveBeenCalledTimes(1);
    });
  });

});