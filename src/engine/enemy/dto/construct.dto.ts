import { EntityDto } from '../../entity/dto/entity.dto';

export type EnemyConstructDto = EntityDto & {
  health: number;
  damage: number;
}