import { Entity } from '../entity/entity';
import { EnemyConstructDto } from './dto/construct.dto';
import { TilemapObjects } from '../mapping/tilemap/tilemap.types';

export class Enemy extends Entity {
  public health: number;
  protected damage: number;

  constructor(dto: EnemyConstructDto) {
    super(dto);
    this.health = dto.health;
    this.damage = dto.damage;
  }

  override handleMovements(tilemapObjects: TilemapObjects, frameCount: number): void {
    return this.animate(frameCount, this.idleAnimation);
  }

  public getDamage(attackerPos: { x: number, y: number }, damage: number, frameCount: number) {
    if (this.lastDamaged + this.invincibilityFrame > frameCount) return;
    this.lastDamaged = frameCount;
    this.health = this.health - damage;

    if (this.health <= 0) {
      this.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
      this.canvas.remove();
    }

    const xDir = attackerPos.x - this.x;
    const yDIr = attackerPos.y - this.y;

    if (xDir > 0) this._x = this._x - this._width;
    else this._x = this._x + this._width;

    if (yDIr > 0) this._y = this._y - this._height;
    else this._y = this._y + this._height;
  }
}