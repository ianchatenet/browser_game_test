import '../index.css';

import tilemaps from './tilemaps';
import { player } from './player';
import { Game } from '../engine/game/game';

window.onload = function () {
  const canvas = document.createElement('canvas');
  canvas.id = 'main';
  document.body.appendChild(canvas);
  new Game(player, tilemaps).init();
};