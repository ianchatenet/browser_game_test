import { Slime } from '../../slime/slime';
import { Sprite } from '../../../engine/sprite/sprite';
import { EnemyConstructDto } from '../../../engine/enemy/dto/construct.dto';
import { EntityMovingAnimation } from '../../../engine/entity/dto/entity.dto';
import {
  idleSlimeGSprite0,
  idleSlimeGSprite1,
  idleSlimeGSprite2,
  movingSlimeGSprite0,
  movingSlimeGSprite1,
  movingSlimeGSprite2,
} from '../../sprites';

const idleSprites: Sprite[] = [idleSlimeGSprite0, idleSlimeGSprite1, idleSlimeGSprite2];
const movingSprites: Sprite[] = [movingSlimeGSprite0, movingSlimeGSprite1, movingSlimeGSprite2];

const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };
const movingAnimation: EntityMovingAnimation = { sprites: movingSprites, frameLag: 8 };

const dto: EnemyConstructDto = {
  speed: 3,
  health: 2,
  damage: 1,
  idleAnimation: idleAnimation,
  downMovingAnimation: movingAnimation,
  leftMovingAnimation: movingAnimation,
  rightMovingAnimation: movingAnimation,
  upMovingAnimation: movingAnimation
};

const enemy1 = new Slime(dto);

const enemy2 = new Slime(dto);

export {
  enemy1,
  enemy2
};