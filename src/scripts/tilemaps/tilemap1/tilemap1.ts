import { enemy1, enemy2 } from './enemies';
import { Wall } from '../../../engine/mapping/wall/wall';
import { Floor } from '../../../engine/mapping/floor/floor';
import { Layer } from '../../../engine/mapping/types/layer';
import { Tilemap } from '../../../engine/mapping/tilemap/tilemap';
import { EnemyTilemap } from '../../../engine/mapping/types/enemy';
import {
  floorSprite1,
  pathBotSprite1,
  pathCornerTopRightSprite1,
  pathLeftSprite1,
  pathRightSprite1,
  pathSprite1,
  pathTopSprite1,
  rockSprite1,
  stumpSprite1,
  wallBotRigthSprite1,
  wallBotSprite1,
  wallRightSprite1,
  wallSprite1,
  wallTopRightSprite1,
  wallTopSprite1,
} from '../../sprites';

const floor1 = new Floor(floorSprite1);

const wallTop1 = new Wall(wallTopSprite1);
const wallTopRight1 = new Wall(wallTopRightSprite1);

const wall1 = new Wall(wallSprite1);
const wallRight1 = new Wall(wallRightSprite1);

const wallBot1 = new Wall(wallBotSprite1);
const wallBotRight1 = new Wall(wallBotRigthSprite1);

const rock1 = new Wall(rockSprite1);

const pathTop1 = new Floor(pathTopSprite1);
const pathBot1 = new Floor(pathBotSprite1);

const pathLeft1 = new Floor(pathLeftSprite1);
const pathRight1 = new Floor(pathRightSprite1);
const pathInvCornerLeft1 = new Floor(pathSprite1);

const pathCornerTopRight = new Floor(pathCornerTopRightSprite1);

const propStump1 = new Wall(stumpSprite1);

const layer1: Layer = [
  [wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1, wallBot1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [pathBot1, pathBot1, pathBot1, pathBot1, pathBot1, pathBot1, pathInvCornerLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [pathTop1, pathTop1, pathTop1, pathTop1, pathTop1, pathCornerTopRight, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [null, null, null, null, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [null, null, null, null, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [null, null, null, null, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, pathRight1, pathLeft1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
  [floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1, floor1],
];

const layer2: Layer = [
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, rock1, null, null, null, propStump1, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [wallTop1, wallTop1, wallTop1, wallTop1, wallTopRight1, null, null, null, null, null, null, null, null, null, null, null],
  [wall1, wall1, wall1, wall1, wallRight1, null, null, null, null, null, null, null, null, null, null, null],
  [wallBot1, wallBot1, wallBot1, wallBot1, wallBotRight1, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
];

const enemiesTilemap: EnemyTilemap[] = [
  {
    entity: enemy1,
    basePosition: { colX: 7, colY: 8 }
  },
  {
    entity: enemy2,
    basePosition: { colX: 10, colY: 12 }
  }
];

const tilemap1 = new Tilemap('firstLevel', [layer1, layer2], enemiesTilemap);

export default tilemap1;