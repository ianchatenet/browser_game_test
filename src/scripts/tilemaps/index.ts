import { Tilemap } from '../../engine/mapping/tilemap/tilemap';

import tilemap1 from './tilemap1/tilemap1';


const tilemaps: Tilemap[] = [tilemap1];
export default tilemaps;