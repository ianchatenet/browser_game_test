/**
 * Describe the position in the canvas
 */
export type Wall = { x: number, y: number }