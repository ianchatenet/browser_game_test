import { Enemy } from '../../engine/enemy/enemy';
import { EnemyConstructDto } from '../../engine/enemy/dto/construct.dto';
import { TilemapObjects } from '../../engine/mapping/tilemap/tilemap.types';

export class Slime extends Enemy {

  private directions: string[] = ['up', 'down', 'left', 'right', 'idle'];
  private directionIndex = 0;

  private directionChanged = 0;

  constructor(dto: EnemyConstructDto) {
    super(dto);
  }

  override handleMovements(tilemapObjects: TilemapObjects, frameCount: number): void {

    // add delay beetween each direction changes

    if (frameCount - this.directionChanged - this.currentAnimation.frameLag * 2 >= this.currentAnimation.frameLag) {
      // add idle behavior beetween each movements 
      if (this.directionIndex === 4) {
        this.directionIndex = Math.floor(Math.random() * this.directions.length);
      } else {
        this.directionIndex = 4;
      }
      this.directionChanged = frameCount;
    }

    const newDir: string = this.directions[this.directionIndex];

    switch (newDir) {
      case this.directions[0]:
        this.handleMoveUp(frameCount, tilemapObjects);
        break;
      case this.directions[1]:
        this.handleMoveDown(frameCount, tilemapObjects);
        break;
      case this.directions[2]:
        this.handleMoveLeft(frameCount, tilemapObjects);
        break;
      case this.directions[3]:
        this.handleMoveRight(frameCount, tilemapObjects);
        break;
      case this.directions[4]:
        this.handleIdle(frameCount);
        break;
    }
  }

  private checkCollision(x: number, y: number, tilemapObjects: TilemapObjects): boolean {
    const { walls, enemies, player } = tilemapObjects;

    for (let i = 0; i < walls.length; i++) {
      const wall = walls[i];

      const collidWall = this.checkPosition(x, y, wall.x, wall.y);
      if (collidWall) return false;
    }

    for (let i = 0; i < enemies.length; i++) {
      const enemy = enemies[i];

      const collidEnemy = this.checkPosition(x, y, enemy.x, enemy.y);
      if (collidEnemy) return false;
    }

    const collidPlayer = this.checkPosition(x, y, player.x, player.y);
    if (collidPlayer) return false;

    return true;
  }

  private handleMoveRight(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkCollision(this._x + this.velocity, this._y, tilemapObjects)) this._x += this.velocity;
    this.animate(frameCount, this.rightMovingAnimation);
  }

  private handleMoveLeft(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkCollision(this._x - this.velocity, this._y, tilemapObjects)) this._x -= this.velocity;
    this.animate(frameCount, this.leftMovingAnimation);
  }

  private handleMoveUp(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkCollision(this._x, this._y - this.velocity, tilemapObjects)) this._y -= this.velocity;
    this.animate(frameCount, this.upMovingAnimation);
  }

  private handleMoveDown(frameCount: number, tilemapObjects: TilemapObjects) {
    if (this.checkCollision(this._x, this._y + this.velocity, tilemapObjects)) this._y += this.velocity;
    this.animate(frameCount, this.downMovingAnimation);
  }

  private handleIdle(frameCount: number) {
    this.animate(frameCount, this.idleAnimation);
  }

  private checkPosition(x: number, y: number, xColid: number, yColid: number): boolean {
    const isInX: boolean = x + this.width > xColid && x < xColid + this.tileWidth;
    const isInY: boolean = y + this.height > yColid && y < yColid + this.tileHeight;
    return isInX && isInY;
  }
}