import { Slime } from './slime';
import { Sprite } from '../../engine/sprite/sprite';
import { Player } from '../../engine/player/player';
import { PlayerDto } from '../../engine/player/dtos/player.dto';
import { EnemyConstructDto } from '../../engine/enemy/dto/construct.dto';
import { EntityAttackAnimation, EntityMovingAnimation } from '../../engine/entity/dto/entity.dto';
import { TilemapObjects } from '../../engine/mapping/tilemap/tilemap.types';

/* eslint-disable @typescript-eslint/no-explicit-any */
describe('Slime', () => {

  const idleSlimeGSprite0 = new Sprite({ sourceImg: '', col: 0, row: 2, height: 16, width: 16, separator: 0 });
  const idleSprites: Sprite[] = [idleSlimeGSprite0];
  const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };
  const attackAnimation: EntityAttackAnimation = { sprites: [idleSlimeGSprite0, idleSlimeGSprite0], frameLag: 8 };
  const dto: EnemyConstructDto = {
    speed: 3,
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    upMovingAnimation: idleAnimation,
    damage: 1,
    health: 10
  };

  const playerDto: PlayerDto = {
    idleAnimation: idleAnimation,
    downMovingAnimation: idleAnimation,
    leftMovingAnimation: idleAnimation,
    rightMovingAnimation: idleAnimation,
    upMovingAnimation: idleAnimation,
    downAttackAnimation: attackAnimation,
    leftAttackAnimation: attackAnimation,
    rightAttackAnimation: attackAnimation,
    upAttackAnimation: attackAnimation,
    speed: 2,
    damage: 1,
    health: 10
  };

  const slime = new Slime(dto);
  const slime2 = new Slime(dto);
  const player = new Player(playerDto);

  const spyRand = jest.spyOn(global.Math, 'random');
  const spyHandleMoveUp = jest.spyOn(slime as any, 'handleMoveUp');
  const spyHandleMoveDown = jest.spyOn(slime as any, 'handleMoveDown');
  const spyHandleMoveLeft = jest.spyOn(slime as any, 'handleMoveLeft');
  const spyHandleMoveRight = jest.spyOn(slime as any, 'handleMoveRight');
  const spyHandleIdle = jest.spyOn(slime as any, 'handleIdle');
  const spyCheckCollision = jest.spyOn(slime as any, 'checkCollision');
  const spyAnimate = jest.spyOn(slime as any, 'animate');

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('#handleMovements', () => {

    it('Should pick up direction and call handleMoveUp', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };

      const frameCount = 24;

      spyRand.mockReturnValueOnce(0);
      spyHandleMoveUp.mockReturnValueOnce({});
      // act
      slime['directionIndex'] = 4;
      slime.handleMovements(tilemapObjects, frameCount);
      // assert
      expect(spyHandleMoveUp).toHaveBeenCalledTimes(1);
    });

    it('Should pick down direction and call handleMoveDown', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };

      const frameCount = 24;

      spyRand.mockReturnValueOnce(1 / 4);
      spyHandleMoveDown.mockReturnValueOnce({});
      // act
      slime['directionIndex'] = 4;
      slime['directionChanged'] = 0;
      slime.handleMovements(tilemapObjects, frameCount);
      // assert
      expect(spyHandleMoveDown).toHaveBeenCalledTimes(1);
    });

    it('Should pick left direction and call handleMoveLeft', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };

      const frameCount = 24;

      spyRand.mockReturnValueOnce(2 / 4);
      spyHandleMoveLeft.mockReturnValueOnce({});
      // act
      slime['directionIndex'] = 4;
      slime['directionChanged'] = 0;
      slime.handleMovements(tilemapObjects, frameCount);
      // assert
      expect(spyHandleMoveLeft).toHaveBeenCalledTimes(1);
    });

    it('Should pick right direction and call handleMoveRight', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };

      const frameCount = 24;

      spyRand.mockReturnValueOnce(3 / 4);
      spyHandleMoveRight.mockReturnValueOnce({});
      // act
      slime['directionIndex'] = 4;
      slime['directionChanged'] = 0;
      slime.handleMovements(tilemapObjects, frameCount);
      // assert
      expect(spyHandleMoveRight).toHaveBeenCalledTimes(1);
    });

    it('Should pick idle direction and call handleIdle', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };

      const frameCount = 24;

      spyRand.mockReturnValueOnce(4 / 4);
      spyHandleIdle.mockReturnValueOnce({});
      // act
      slime['directionIndex'] = 1;
      slime['directionChanged'] = 0;
      slime.handleMovements(tilemapObjects, frameCount);
      // assert
      expect(spyHandleIdle).toHaveBeenCalledTimes(1);
    });
  });

  describe('#checkCollision', () => {

    slime['_width'] = 16;
    slime['_height'] = 16;
    slime['tileWidth'] = 16;
    slime['tileHeight'] = 16;

    it('Should return true because do not collide', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };
      // act
      const result: boolean = slime['checkCollision'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(true);
    });

    it('Should return false because collide with a wall', () => {
      // arrange
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: [
          {
            x: 0,
            y: 0
          }
        ]
      };
      // act
      const result: boolean = slime['checkCollision'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because collide with an other enemy', () => {
      // arrange
      slime2['_width'] = 16;
      slime2['_height'] = 16;
      slime2['_x'] = 0;
      slime2['_y'] = 0;
      const tilemapObjects: TilemapObjects = {
        enemies: [slime2],
        player: player,
        walls: []
      };
      // act
      const result: boolean = slime['checkCollision'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(false);
    });

    it('Should return false because collide with player', () => {
      // arrange
      player['_width'] = 16;
      player['_height'] = 16;
      player['_x'] = 0;
      player['_y'] = 0;
      const tilemapObjects: TilemapObjects = {
        enemies: [],
        player: player,
        walls: []
      };
      // act
      const result: boolean = slime['checkCollision'](0, 0, tilemapObjects);
      // assert
      expect(result).toStrictEqual(false);
    });
  });

  describe('#handleMoveRight', () => {

    const tilemapObjects: TilemapObjects = {
      enemies: [],
      player: player,
      walls: []
    };

    it('Should move right', () => {
      // arrange
      const baseX = slime['x'];
      spyCheckCollision.mockReturnValueOnce(true);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveRight'](16, tilemapObjects);
      // assert
      expect(slime['x']).toStrictEqual(baseX + slime['velocity']);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

    it('Should not move right', () => {
      // arrange
      const baseX = slime['x'];
      spyCheckCollision.mockReturnValueOnce(false);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveRight'](16, tilemapObjects);
      // assert
      expect(slime['x']).toStrictEqual(baseX);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

  });

  describe('#handleMoveLeft', () => {

    const tilemapObjects: TilemapObjects = {
      enemies: [],
      player: player,
      walls: []
    };

    it('Should move left', () => {
      // arrange
      const baseX = slime['x'];
      spyCheckCollision.mockReturnValueOnce(true);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveLeft'](16, tilemapObjects);
      // assert
      expect(slime['x']).toStrictEqual(baseX - slime['velocity']);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

    it('Should not move left', () => {
      // arrange
      const baseX = slime['x'];
      spyCheckCollision.mockReturnValueOnce(false);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveLeft'](16, tilemapObjects);
      // assert
      expect(slime['x']).toStrictEqual(baseX);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

  });

  describe('#handleMoveUp', () => {

    const tilemapObjects: TilemapObjects = {
      enemies: [],
      player: player,
      walls: []
    };

    it('Should move up', () => {
      // arrange
      const baseY = slime['y'];
      spyCheckCollision.mockReturnValueOnce(true);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveUp'](16, tilemapObjects);
      // assert
      expect(slime['y']).toStrictEqual(baseY + slime['velocity']);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

    it('Should not move up', () => {
      // arrange
      const baseY = slime['y'];
      spyCheckCollision.mockReturnValueOnce(false);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveUp'](16, tilemapObjects);
      // assert
      expect(slime['y']).toStrictEqual(baseY);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

  });

  describe('#handleMoveDown', () => {

    const tilemapObjects: TilemapObjects = {
      enemies: [],
      player: player,
      walls: []
    };

    it('Should move down', () => {
      // arrange
      const baseY = slime['y'];
      spyCheckCollision.mockReturnValueOnce(true);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveDown'](16, tilemapObjects);
      // assert
      expect(slime['y']).toStrictEqual(baseY - slime['velocity']);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

    it('Should not move down', () => {
      // arrange
      const baseY = slime['y'];
      spyCheckCollision.mockReturnValueOnce(false);
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleMoveDown'](16, tilemapObjects);
      // assert
      expect(slime['y']).toStrictEqual(baseY);
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

  });

  describe('#handleIdle', () => {

    it('Should call animate', () => {
      // arrange
      spyAnimate.mockReturnValueOnce(undefined);
      // act
      slime['handleIdle'](16);
      // assert
      expect(spyAnimate).toHaveBeenCalledTimes(1);
    });

  });
});