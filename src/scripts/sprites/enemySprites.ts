import SlimeImg from '../../images/slimeTiles.png';
import { Sprite } from '../../engine/sprite/sprite';

const idleSlimeGSprite0 = new Sprite({ sourceImg: SlimeImg, col: 0, row: 2, height: 16, width: 16, separator: 0 });
const idleSlimeGSprite1 = new Sprite({ sourceImg: SlimeImg, col: 1, row: 2, height: 16, width: 16, separator: 0 });
const idleSlimeGSprite2 = new Sprite({ sourceImg: SlimeImg, col: 2, row: 2, height: 16, width: 16, separator: 0 });

const movingSlimeGSprite0 = new Sprite({ sourceImg: SlimeImg, col: 3, row: 2, height: 16, width: 16, separator: 0 });
const movingSlimeGSprite1 = new Sprite({ sourceImg: SlimeImg, col: 4, row: 2, height: 16, width: 16, separator: 0 });
const movingSlimeGSprite2 = new Sprite({ sourceImg: SlimeImg, col: 5, row: 2, height: 16, width: 16, separator: 0 });

export {
  idleSlimeGSprite0, idleSlimeGSprite1, idleSlimeGSprite2, movingSlimeGSprite0, movingSlimeGSprite1, movingSlimeGSprite2
};