import PropsImg from '../../images/props.png';
import { Sprite } from '../../engine/sprite/sprite';

const rockSprite1 = new Sprite({ sourceImg: PropsImg, col: 0, row: 12, height: 16, width: 16, separator: 0 });

export * from './propSprites';
export * from './wallSprites';
export * from './pathSprites';
export * from './floorSprites';
export * from './playerSprites';
export * from './enemySprites';

export {
  rockSprite1,
};