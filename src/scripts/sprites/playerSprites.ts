import HeroImg from '../../images/heroTiles.png';
import { Sprite } from '../../engine/sprite/sprite';

const idlePlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 0, row: 1, height: 16, width: 16, separator: 0 });
const idlePlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 1, row: 1, height: 16, width: 16, separator: 0 });
const idlePlayerSprite2 = new Sprite({ sourceImg: HeroImg, col: 2, row: 1, height: 16, width: 16, separator: 0 });

const moveRightPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 0, row: 5, height: 16, width: 16, separator: 0 });
const moveRightPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 1, row: 5, height: 16, width: 16, separator: 0 });
const moveRightPlayerSprite2 = new Sprite({ sourceImg: HeroImg, col: 2, row: 5, height: 16, width: 16, separator: 0 });
const moveRightPlayerSprite3 = new Sprite({ sourceImg: HeroImg, col: 3, row: 5, height: 16, width: 16, separator: 0 });

const moveLeftPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 4, row: 5, height: 16, width: 16, separator: 0 });
const moveLeftPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 5, row: 5, height: 16, width: 16, separator: 0 });
const moveLeftPlayerSprite2 = new Sprite({ sourceImg: HeroImg, col: 6, row: 5, height: 16, width: 16, separator: 0 });
const moveLeftPlayerSprite3 = new Sprite({ sourceImg: HeroImg, col: 7, row: 5, height: 16, width: 16, separator: 0 });

const moveUpPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 4, row: 4, height: 16, width: 16, separator: 0 });
const moveUpPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 5, row: 4, height: 16, width: 16, separator: 0 });
const moveUpPlayerSprite2 = new Sprite({ sourceImg: HeroImg, col: 6, row: 4, height: 16, width: 16, separator: 0 });
const moveUpPlayerSprite3 = new Sprite({ sourceImg: HeroImg, col: 7, row: 4, height: 16, width: 16, separator: 0 });

const moveDownPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 0, row: 4, height: 16, width: 16, separator: 0 });
const moveDownPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 1, row: 4, height: 16, width: 16, separator: 0 });
const moveDownPlayerSprite2 = new Sprite({ sourceImg: HeroImg, col: 2, row: 4, height: 16, width: 16, separator: 0 });
const moveDownPlayerSprite3 = new Sprite({ sourceImg: HeroImg, col: 3, row: 4, height: 16, width: 16, separator: 0 });

const attackUpPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 5, row: 1, height: 16, width: 16, separator: 0 });
const attackUpPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 5, row: 0, height: 16, width: 16, separator: 0 });

const attackDownPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 3, row: 1, height: 16, width: 16, separator: 0 });
const attackDownPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 3, row: 2, height: 16, width: 16, separator: 0 });

const attackRightPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 4, row: 3, height: 16, width: 16, separator: 0 });
const attackRightPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 5, row: 3, height: 16, width: 16, separator: 0 });

const attackLeftPlayerSprite0 = new Sprite({ sourceImg: HeroImg, col: 2, row: 3, height: 16, width: 16, separator: 0 });
const attackLeftPlayerSprite1 = new Sprite({ sourceImg: HeroImg, col: 1, row: 3, height: 16, width: 16, separator: 0 });

export {
  idlePlayerSprite0,
  idlePlayerSprite1,
  idlePlayerSprite2,
  moveRightPlayerSprite0,
  moveRightPlayerSprite1,
  moveRightPlayerSprite2,
  moveRightPlayerSprite3,
  moveLeftPlayerSprite0,
  moveLeftPlayerSprite1,
  moveLeftPlayerSprite2,
  moveLeftPlayerSprite3,
  moveUpPlayerSprite0,
  moveUpPlayerSprite1,
  moveUpPlayerSprite2,
  moveUpPlayerSprite3,
  moveDownPlayerSprite0,
  moveDownPlayerSprite1,
  moveDownPlayerSprite2,
  moveDownPlayerSprite3,
  attackUpPlayerSprite0,
  attackUpPlayerSprite1,
  attackDownPlayerSprite0,
  attackDownPlayerSprite1,
  attackRightPlayerSprite0,
  attackRightPlayerSprite1,
  attackLeftPlayerSprite0,
  attackLeftPlayerSprite1
};