import { Sprite } from '../../engine/sprite/sprite';
import TilemapImg from '../../images/baseTiles.png';

const floorSprite1 = new Sprite({ sourceImg: TilemapImg, col: 5, row: 1, height: 16, width: 16, separator: 0 });

export {
  floorSprite1
};