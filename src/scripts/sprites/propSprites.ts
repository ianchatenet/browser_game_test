import PropsImg from '../../images/props.png';
import { Sprite } from '../../engine/sprite/sprite';

const stumpSprite1 = new Sprite({ sourceImg: PropsImg, col: 0, row: 8, height: 16, width: 16, separator: 0 });

export {
  stumpSprite1
};