import { Sprite } from '../../engine/sprite/sprite';
import TilemapImg from '../../images/baseTiles.png';

const pathTopSprite1 = new Sprite({ sourceImg: TilemapImg, col: 6, row: 9, height: 16, width: 16, separator: 0 });
const pathSprite1 = new Sprite({ sourceImg: TilemapImg, col: 16, row: 10, height: 16, width: 16, separator: 0 });
const pathBotSprite1 = new Sprite({ sourceImg: TilemapImg, col: 6, row: 11, height: 16, width: 16, separator: 0 });

const pathLeftSprite1 = new Sprite({ sourceImg: TilemapImg, col: 5, row: 10, height: 16, width: 16, separator: 0 });
const pathRightSprite1 = new Sprite({ sourceImg: TilemapImg, col: 7, row: 10, height: 16, width: 16, separator: 0 });

const pathCornerTopRightSprite1 = new Sprite({ sourceImg: TilemapImg, col: 7, row: 9, height: 16, width: 16, separator: 0 });

export {
  pathTopSprite1,
  pathSprite1,
  pathBotSprite1,
  pathLeftSprite1,
  pathRightSprite1,
  pathCornerTopRightSprite1
};