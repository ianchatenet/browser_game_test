import { Sprite } from '../../engine/sprite/sprite';
import TilemapImg from '../../images/baseTiles.png';

const wallTopSprite1 = new Sprite({ sourceImg: TilemapImg, col: 5, row: 2, height: 16, width: 16, separator: 0 });
const wallTopRightSprite1 = new Sprite({ sourceImg: TilemapImg, col: 6, row: 2, height: 16, width: 16, separator: 0 });
const wallSprite1 = new Sprite({ sourceImg: TilemapImg, col: 5, row: 3, height: 16, width: 16, separator: 0 });
const wallRightSprite1 = new Sprite({ sourceImg: TilemapImg, col: 6, row: 3, height: 16, width: 16, separator: 0 });
const wallBotSprite1 = new Sprite({ sourceImg: TilemapImg, col: 5, row: 4, height: 16, width: 16, separator: 0 });
const wallBotRigthSprite1 = new Sprite({ sourceImg: TilemapImg, col: 6, row: 4, height: 16, width: 16, separator: 0 });

export {
  wallTopSprite1,
  wallTopRightSprite1,
  wallSprite1,
  wallRightSprite1,
  wallBotSprite1,
  wallBotRigthSprite1
};