import { Player } from '../engine/player/player';
import { Sprite } from '../engine/sprite/sprite';
import { PlayerDto } from '../engine/player/dtos/player.dto';
import { EntityAttackAnimation, EntityMovingAnimation } from '../engine/entity/dto/entity.dto';
import {
  attackDownPlayerSprite0,
  attackDownPlayerSprite1,
  attackLeftPlayerSprite0,
  attackLeftPlayerSprite1,
  attackRightPlayerSprite0,
  attackRightPlayerSprite1,
  attackUpPlayerSprite0,
  attackUpPlayerSprite1,
  idlePlayerSprite0,
  idlePlayerSprite1,
  idlePlayerSprite2,
  moveDownPlayerSprite0,
  moveDownPlayerSprite1,
  moveDownPlayerSprite2,
  moveDownPlayerSprite3,
  moveLeftPlayerSprite0,
  moveLeftPlayerSprite1,
  moveLeftPlayerSprite2,
  moveLeftPlayerSprite3,
  moveRightPlayerSprite0,
  moveRightPlayerSprite1,
  moveRightPlayerSprite2,
  moveRightPlayerSprite3,
  moveUpPlayerSprite0,
  moveUpPlayerSprite1,
  moveUpPlayerSprite2,
  moveUpPlayerSprite3,
} from './sprites';

const idleSprites: Sprite[] = [idlePlayerSprite0, idlePlayerSprite1, idlePlayerSprite2];
const idleAnimation: EntityMovingAnimation = { sprites: idleSprites, frameLag: 8 };

const movingRightSprites: Sprite[] = [moveRightPlayerSprite0, moveRightPlayerSprite1, moveRightPlayerSprite2, moveRightPlayerSprite3];
const movingRightAnimation: EntityMovingAnimation = { sprites: movingRightSprites, frameLag: 8 };

const movingLeftSprites: Sprite[] = [moveLeftPlayerSprite0, moveLeftPlayerSprite1, moveLeftPlayerSprite2, moveLeftPlayerSprite3];
const movingLeftAnimation: EntityMovingAnimation = { sprites: movingLeftSprites, frameLag: 8 };

const movingUpSprites: Sprite[] = [moveUpPlayerSprite0, moveUpPlayerSprite1, moveUpPlayerSprite2, moveUpPlayerSprite3];
const movingUpAnimation: EntityMovingAnimation = { sprites: movingUpSprites, frameLag: 8 };

const movingDownSprites: Sprite[] = [moveDownPlayerSprite0, moveDownPlayerSprite1, moveDownPlayerSprite2, moveDownPlayerSprite3];
const movingDownAnimation: EntityMovingAnimation = { sprites: movingDownSprites, frameLag: 8 };

const attackUpSprites: [Sprite, Sprite] = [attackUpPlayerSprite0, attackUpPlayerSprite1];
const attackUpAnimation: EntityAttackAnimation = { sprites: attackUpSprites, frameLag: 8 };

const attackDownSprites: [Sprite, Sprite] = [attackDownPlayerSprite0, attackDownPlayerSprite1];
const attackDownAnimation: EntityAttackAnimation = { sprites: attackDownSprites, frameLag: 8 };

const attackRightSprites: [Sprite, Sprite] = [attackRightPlayerSprite0, attackRightPlayerSprite1];
const attackRightAnimation: EntityAttackAnimation = { sprites: attackRightSprites, frameLag: 8 };

const attackLeftSprites: [Sprite, Sprite] = [attackLeftPlayerSprite0, attackLeftPlayerSprite1];
const attackLeftAnimation: EntityAttackAnimation = { sprites: attackLeftSprites, frameLag: 8 };

const playerDto: PlayerDto = {
  speed: 3,
  health: 10,
  damage: 1,
  idleAnimation: idleAnimation,
  rightMovingAnimation: movingRightAnimation,
  leftMovingAnimation: movingLeftAnimation,
  upMovingAnimation: movingUpAnimation,
  downMovingAnimation: movingDownAnimation,
  leftAttackAnimation: attackLeftAnimation,
  downAttackAnimation: attackDownAnimation,
  rightAttackAnimation: attackRightAnimation,
  upAttackAnimation: attackUpAnimation
};

const player = new Player(playerDto);

export { player };