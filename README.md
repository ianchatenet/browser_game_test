Browser game learning project

## How to

`npm i`
`npm buid` build assets.
`npm buid:w` build assets in watch mode.
`npm serve` build and serve projects on port 8000.

http://localhost:8000/ to access the server

open `dist/index.html`

## Docker

### Run the project in a container:

`docker build -t game -f Dockerfile .` Build the image.

`docker run -it --rm -p 8000:8000 -v %cd%:/usr/src game` Run the image using **cmd**.

`docker run -it --rm -p 8000:8000 -v ${PWD}:/usr/src game` Run the image using **PowerShell**.

`docker run -it --rm -p 8000:8000 -v "$(pwd -W)":/usr/src game` Run the image using **bash**.

http://localhost:8000/ to access the server


## Gitlab ci

Pushing on branch `develop` deploy the project on https://ianchatenet.gitlab.io/browser_game_test/
