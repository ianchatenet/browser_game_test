FROM node:16-alpine

WORKDIR /usr/src

RUN apk add --update --no-cache \
  make \
  g++ \
  jpeg-dev \
  cairo-dev \
  giflib-dev \
  pango-dev

ENTRYPOINT npm install && npm run serve

EXPOSE 8000