module.exports = {
  'preset': 'ts-jest',
  'testEnvironment': 'jsdom',
  'testMatch': [
    '**/*.test.ts',
  ],
  'coveragePathIgnorePatterns': [
    'node_modules',
    'dist'
  ],
  'watchPathIgnorePatterns': [
    'node_modules',
    'dist'
  ],
  'coverageThreshold': {
    'global': {
      'branches': 80,
      'functions': 80,
      'lines': 80,
      'statements': -10
    }
  },
  'globals': {
    'ts-jest': {
      'ignoreCoverageForDecorators': true,
      'ignoreCoverageForAllDecorators': true
    }
  },
  "moduleNameMapper": {
    "\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js"
  }
};
